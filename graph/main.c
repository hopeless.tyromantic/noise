#include "../quiaf/quiaf.h"

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>

#define WIDTH 1920
#define HEIGHT 512

#define SAMPLE_RATE 16000

Display* dis;
Window win;
GC gc;
int screen;
XImage* image;

static uint32_t fb[HEIGHT][WIDTH];


int die(char* s, ...) {
    va_list v;
    va_start(v, s);
    vfprintf(stderr, s, v);
    fputc('\n', stderr);
    exit(1);
    va_end(v);
    return 1;
}

void xinit() {
	dis = XOpenDisplay(NULL);
	screen = DefaultScreen(dis);
	win = XCreateSimpleWindow(dis, RootWindow(dis, screen), 0, 0, WIDTH, HEIGHT, 0,
							BlackPixel(dis, screen), WhitePixel(dis, screen));

	XSelectInput(dis, win, KeyPressMask | KeyReleaseMask);

    int depth = DefaultDepth(dis, screen);

	fprintf(stderr, "Depth: %d\n", depth);

	image = XCreateImage(dis, CopyFromParent, depth, ZPixmap, 0, 0, WIDTH, HEIGHT, 32, WIDTH*4);
	image->data = (char*) fb;

	XClearWindow(dis, win);
	XMapRaised(dis, win);
	XFlush(dis);

	gc = XCreateGC(dis, win, 0, 0);

	XClassHint xclass = {
		"qh4osgame",
		"beeiods"
	};

	XSetClassHint(dis, win, &xclass);
}

#define data(c,n) _data[c*n_samples + n]


int main(int argc, char* argv[]) {
    if(argc < 2) {
        die("expected file, none provided");
    }

    FILE* in = fopen(argv[1], "rb");

    struct QUIAF* q = QUIAF_read(in);

    if(!q) {
        die("unable to process \"%s\"", argv[1]);
    }

    printf("Number of samples: %u\n", q->header.n_samples);

    /* cut ourselves down to SAMPLE_RATE, single channel */
    size_t old_samples_per_new = (q->header.sample_rate)/(float)SAMPLE_RATE;
    size_t n_samples = q->header.n_samples/old_samples_per_new;
    printf("Number of new samples: %zu\n", n_samples);
    uint32_t* _data = calloc(q->header.n_channels * sizeof(uint32_t), n_samples);

    fprintf(stderr, "%p\n", _data);

    printf("%zu\n", old_samples_per_new);
    for(size_t c = 0; c < q->header.n_channels; c++) {
        size_t idx = 0;
        for(size_t i = 0; i < n_samples; i++) {
            float sample = 0.0;
            for(size_t j = 0; j < old_samples_per_new; j++) {
                sample += q->samples[c][idx + j];
            }
            data(c,i) = ((sample/old_samples_per_new/2.0) + 0.5) * HEIGHT / q->header.n_channels;
            idx += old_samples_per_new;
        }
    }


    xinit();

    free(q->samples);

    XPutImage(dis, win, gc, image, 0, 0, 0, 0, WIDTH, HEIGHT);


    int i = 0;
	while(1) {
        bzero(fb, sizeof(fb));

        for(size_t c = 0; c < q->header.n_channels; c++) {
            for(size_t x = 0; x < WIDTH; x++) {
                size_t idx = data(c,x+i);
                idx /= q->header.n_channels;
                idx += HEIGHT/q->header.n_channels*c;
                fb[idx][x] = ~0;
            }
        }

        i+=5;
		XEvent e;

        XPutImage(dis, win, gc, image, 0, 0, 0, 0, WIDTH, HEIGHT);
		
		while(XCheckWindowEvent(dis, win, KeyPressMask, &e)) {
			switch(e.type) {
				case KeyPress:
					
				break;
			}
		}
    }

    fclose(in);
}
