#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "quiaf.h"

struct WAVHeader {
    char        magic_RIFF[4];
    uint32_t    fsize;
    uint32_t    magic_WAVE;
    uint32_t    magic_FMT;
    uint32_t    header_len;
    uint16_t    format_type;
    uint16_t    n_channels;
    uint32_t    sample_rate;
    uint32_t    byte_rate;
    uint16_t    block_align;
    uint16_t    bits_per_sample;
};

#define MAX_VALUE (float)(INT16_MAX)
int max = 0;

int die(char* s, ...) {
    va_list v;
    va_start(v, s);
    vfprintf(stderr, s, v);
    exit(1);
    va_end(v);
    return 1;
}

static void QUIAF_alloc(struct QUIAF* q) {
    float* data = calloc(sizeof(float) * q->header.n_channels, q->header.n_samples);

    q->samples = calloc(sizeof(float*), 2);

    for(size_t i = 0; i < q->header.n_channels; i++) {
        q->samples[i] = data + q->header.n_samples*i;
    }
}

static float s16_to_float(int16_t s) {
    return ((float)s) / (float)max;
}

int main(int argc, char* argv[]) {
    struct WAVHeader wavhead;
    FILE* in = fopen(argc>1? argv[1] : "zhivago.wav", "rb");

    fread(&wavhead, sizeof(wavhead), 1, in);

    if(strncmp(wavhead.magic_RIFF, "RIFF", 4)) {
        die("This isn't a WAV file, dumbass");
    }

    /* skip to the data */
    int ch = 0;

    while(1) {
        /* I don't want to have to put stuff back in the stream, sue me */
        if(ch == 'd') {
            ch = getc(in);
            if(ch == 'a') {
                ch = getc(in);
                if(ch == 't') {
                    ch = getc(in);
                    if(ch == 'a') {
                        break;
                    }
                }
            }
        } else {
            ch = getc(in);
        }
    }

    uint32_t data_size = 0;
    fread(&data_size, 4, 1, in);

    printf("Header: ");
    fwrite((char*)&wavhead.magic_RIFF, 4, 1, stdout);
    printf("\n"
        "File Size:             %d\n"
        "MAGIC:                 %s\n"
        "Header Length:         %d\n"
        "Format Type:           %d\n"
        "Number of Channels:    %d\n"
        "Sample Rate:           %d\n"
        "Byte Rate:             %d\n"
        "Block Align:           %d\n"
        "Bits per Sample:       %d\n"
        "Data Size:             %d\n",

        wavhead.fsize, (char*)&wavhead.magic_WAVE, wavhead.header_len,
        wavhead.format_type, wavhead.n_channels, wavhead.sample_rate,
        wavhead.byte_rate, wavhead.block_align, wavhead.bits_per_sample,
        data_size
    );

    int16_t* data = calloc(data_size, 1);

    fread(data, data_size, 1, in);

    struct QUIAF q;

    q.header = (struct QUIAF_header){
        .magic          = QUIAF_MAGIC,
        .n_channels     = wavhead.n_channels,
        .n_samples      = data_size/(wavhead.block_align),
        .sample_rate    = wavhead.sample_rate,
    };

    printf("SAMPLE: %u\n", q.header.n_samples);

    QUIAF_alloc(&q);

    if(wavhead.bits_per_sample != 16) {
        die("not ready for %d-bit samples\n", wavhead.bits_per_sample);
    }

    int16_t* sample = data;

    for(size_t i = 0; i < q.header.n_samples; i++) {
        for(size_t j = 0; j < q.header.n_channels; j++) {
            if(*sample > max)
                max = *sample;
            sample++;
        }
    }

    sample = data;
    
    printf("max: %d\n", max);
    for(size_t i = 0; i < q.header.n_samples; i++) {
        for(size_t j = 0; j < q.header.n_channels; j++) {
            q.samples[j][i] = s16_to_float(*sample++);
        }
    }

    FILE* out = fopen("zhivago.qf", "wb");
    QUIAF_write(&q, out);
    fclose(out);

    FILE* in2 = fopen("zhivago.qf", "rb");
    q = *QUIAF_read(in2);
}
