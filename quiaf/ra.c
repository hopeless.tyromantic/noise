#include <math.h>
#include <stdio.h>

#include "quiaf.h"

const float running_away[] = {
    (19.45*4.0), (19.45*4.0), (23.12*8.0), (21.83*8.0),
    0, 0, (19.45*8.0), (19.45*8.0),
    (19.45*8.0), (19.45*8.0), (17.32*8.0), (29.14*4.0),
    (19.45*4.0), (19.45*4.0), (17.32*4.0), (30.87*4.0),

    (27.50*4.0), (27.50*4.0), (29.14*4.0), (27.50*4.0),
    0, 0, (30.87*4.0), (30.87*4.0),
    (30.87*4.0), (30.87*4.0), (19.45*4.0), (19.45*4.0),
    0.0, (17.32*4.0), (19.45*4.0), (17.32*4.0)
};

static void QUIAF_alloc(struct QUIAF* q) {
    float* data = calloc(sizeof(float) * q->header.n_channels, q->header.n_samples);

    q->samples = calloc(sizeof(float*), 2);

    for(size_t i = 0; i < q->header.n_channels; i++) {
        q->samples[i] = data + q->header.n_samples*i;
    }
}

#define SIN_WAVE(pitch, time) sinf(time*pitch*2.0*M_PI)

#define SAMPLE_RATE 48000
#define LENGTH (SAMPLE_RATE*16)

int main() {
    struct QUIAF q;

    q.header = (struct QUIAF_header) {
        .magic          = QUIAF_MAGIC,
        .n_channels     = 1,
        .n_samples      = LENGTH,
        .sample_rate    = SAMPLE_RATE,
    };

    QUIAF_alloc(&q);

    float* data = q.samples[0];

    float time = 0.0;
    /* play each sample for 1/8th of a second, for a total of 4 seconds, repeat 4 times */
    for(size_t i = 0; i < 4; i++) {
        for(size_t j = 0; j < 32; j++) {
            for(size_t k = 0; k < SAMPLE_RATE/8; k++) {
                time += 1.0/(float)SAMPLE_RATE;
                *data++ = SIN_WAVE(running_away[j], time);
            }
        }
    }

    FILE* out = fopen("ra.qf", "wb");
    QUIAF_write(&q, out);
}
