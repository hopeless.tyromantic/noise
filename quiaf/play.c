#include <math.h>

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <sys/time.h>

#include <soundio/soundio.h>

#include "quiaf.h"

struct QUIAF* q;
size_t sample_idx = 0;
size_t end_sample = 0;
int n_channels = 1;
float audio_time;

int die(char* s, ...) {
    va_list v;
    va_start(v, s);
    vfprintf(stderr, s, v);
    fputc('\n', stderr);
    exit(1);
    va_end(v);
    return 1;
}


long time() {
    static long start_time = 0;
    struct timeval time;
    gettimeofday(&time, NULL);
    if(start_time == 0)
        start_time = (time.tv_sec*1000 + time.tv_usec/1000);
    return (time.tv_sec*1000 + time.tv_usec/1000) - start_time;
}

static int min(int x, int y) {
    if(x < y)
        return x;
    else
        return y;
}

static void write_callback(struct SoundIoOutStream* out, int frame_count_min, int frame_count_max) {
    (void) frame_count_min;
    /* shove as many samples out as possible using soundio_oustream_begin_write and "_end_write */

    /* memory buffer? */
    struct SoundIoChannelArea* areas;
    int err;

    int frames_left = min(q->header.n_samples-sample_idx, frame_count_max);

    int first_sample = sample_idx;

    while(frames_left > 0) {
        int frame_count = frames_left;
        if((err = soundio_outstream_begin_write(out, &areas, &frame_count))) {
            fprintf(stderr, "stream error: %s\n", soundio_strerror(err));
            exit(1);
        }

        if(!frame_count)
            break;

        const struct SoundIoChannelLayout* layout = &out->layout;
        for(int frame = 0; frame < frame_count; frame++) {
            if(n_channels == layout->channel_count) {
                for(int i = 0; i < layout->channel_count; i++) {
                    float* ptr = (float*)(areas[i].ptr + areas[i].step * frame);            
                    *ptr = q->samples[i][sample_idx];
                }
            } else if(n_channels > layout->channel_count){
                /* I suppose just average all of them? */
                int32_t sample = 0;
                for(int i = 0; i < n_channels; i++)
                    sample += q->samples[i][sample_idx];
                sample /= n_channels;
                for(int i = 0; i < layout->channel_count; i++) {
                    float* ptr = (float*)(areas[i].ptr + areas[i].step * frame);
                    *ptr = sample;
                }
            } else {
                float sample = q->samples[0][sample_idx];
                for(int i = 0; i < layout->channel_count; i++) {
                    float* ptr = (float*)(areas[i].ptr + areas[i].step * frame);
                    *ptr = sample;
                }
            }
            sample_idx++;
        }
        

        if((err = soundio_outstream_end_write(out))) {
            if(err == SoundIoErrorUnderflow)
                goto end;
            fprintf(stderr, "stream error: %s\n", soundio_strerror(err));
            exit(1);
        }

        frames_left -= frame_count;
    }

    end:
        audio_time += (float)(sample_idx-first_sample)/(float)out->sample_rate/(float)n_channels;
    return;
}

static struct SoundIo* sound_setup(struct SoundIoDevice** _dev, struct SoundIoOutStream** _out, struct QUIAF_header* header) {
	int status;
	struct SoundIo* soundio = soundio_create();
	if(!soundio) {
		perror("soundio");
		exit(1);
	}

	if((status = soundio_connect(soundio))) {
		fprintf(stderr, "error connecting: %s\n", soundio_strerror(status));
		exit(1);
	}

	soundio_flush_events(soundio);

	int default_index = soundio_default_output_device_index(soundio);
	if(default_index < 0) {
		fprintf(stderr, "no output device found\n");
		exit(1);
	}

	struct SoundIoDevice* dev = soundio_get_output_device(soundio, default_index);
	if(!dev) {
		perror("soundio");
		exit(1);
	}

	fprintf(stderr, "Output device bound: %s\n", dev->name);

	struct SoundIoOutStream* out = soundio_outstream_create(dev);
	out->format = SoundIoFormatFloat32NE;
    out->sample_rate = header->sample_rate;
	out->write_callback = write_callback;

    time();

	if((status = soundio_outstream_open(out))) {
		fprintf(stderr, "can't open device: %s\n", soundio_strerror(status));
		exit(1);
	}

	if(_dev)
	    *_dev = dev;
	if(_out)
	    *_out = out;

	return soundio;
}


int main(int argc, char* argv[]) {
    if(argc < 2) {
        die("expected file, none provided");
    }

    FILE* in = fopen(argv[1], "rb");

    q = QUIAF_read(in);

    if(!q) {
        die("unable to process \"%s\"", argv[1]);
    }

    printf("Number of samples: %u\n", q->header.n_samples);

    struct SoundIoDevice* dev;
    struct SoundIoOutStream* out;
    struct SoundIo* soundio = sound_setup(&dev, &out, &q->header);
    int status = 0;

	if((status = soundio_outstream_start(out))) {
		fprintf(stderr, "can't start device: %s\n", soundio_strerror(status));
		exit(1);
	}

    while(end_sample != sample_idx) {
        soundio_flush_events(soundio);
    }

    soundio_outstream_destroy(out);
    soundio_device_unref(dev);
    soundio_destroy(soundio);

}
