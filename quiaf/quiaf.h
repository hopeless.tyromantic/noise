/* QH4OS Understandably Inflexible Audio Format
 *
 * (all data is in 32-bit floats)
 * Structure:
 * +----------+----------------------+
 * | 48 bits  |       "QUAIF\0"      |
 * +----------+----------------------+
 * | 16 bits  |  Number of Channels  |
 * +----------+----------------------+
 * | 32 bits  |  Number of Samples   |
 * +----------+----------------------+
 * | 32 bits  |       Sample Rate    |
 * +----------+----------------------+
 *
 */

#ifndef QUIAF_H_
#define QUIAF_H_

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define QUIAF_MAGIC "QUIAF"

struct QUIAF_header {
    char        magic[6];
    uint16_t    n_channels;
    uint32_t    n_samples;
    uint32_t    sample_rate;
};


/* header followed by samples[n_channels][n_samples] */
struct QUIAF {
    struct QUIAF_header header;
    float**             samples;
};

static bool QUIAF_verify(struct QUIAF_header* q) {
    return (strcmp(QUIAF_MAGIC, q->magic) == 0);
}

static void QUIAF_print_header(struct QUIAF_header* q) {
    fprintf(stderr,
        "MAGIC:         %5s\n"
        "Channels:      %u\n"
        "Samples:       %u\n"
        "Sample Rate:   %u\n",
        q->magic, q->n_channels, q->n_samples, q->sample_rate);
}

static struct QUIAF* QUIAF_read(FILE* in) {
    struct QUIAF* ret = calloc(sizeof(struct QUIAF), 1);
    fread(&ret->header, sizeof(struct QUIAF_header), 1, in);

    if(!QUIAF_verify(&ret->header)) {
        free(ret);
        return NULL;
    }

    float* data = calloc(sizeof(float) * ret->header.n_channels, ret->header.n_samples);

    fread(data, ret->header.n_channels * sizeof(float), ret->header.n_samples, in);

    ret->samples = calloc(sizeof(float*), ret->header.n_channels);

    for(size_t i = 0; i < ret->header.n_channels; i++) {
        ret->samples[i] = data + ret->header.n_samples*i;
    }

    return ret;
}

static void QUIAF_write(struct QUIAF* q, FILE* out) {
    fwrite(&q->header, sizeof(q->header), 1, out);
    fwrite(q->samples[0], sizeof(float), q->header.n_channels*q->header.n_samples, out);
}

#endif
